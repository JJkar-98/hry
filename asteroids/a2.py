import sys, pygame, math
from random import randint, choice, random
pygame.init()
SIZE = WIDTH, HEIGHT = (1000, 1000)
BACKGROUND = (200, 200, 200)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
screen = pygame.display.set_mode(SIZE)
clock = pygame.time.Clock()

class Raketa(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf_original = pygame.image.load("raketa.png").convert()
        self.surf_original.set_colorkey((255,255,255))
        self.rect_original = self.surf_original.get_rect()
        self.rect_original.move_ip(450,450)
        self.uhel = 90.0
        self.rychlost_rotace = 5
        self.rychlost = [0.0, 0.0]
        self.nabito = True
        
    
    def update(self, klavesy, objekty):
        if klavesy[pygame.K_LEFT]:
            self.uhel += self.rychlost_rotace
        if klavesy[pygame.K_RIGHT]:
            self.uhel -= self.rychlost_rotace
        if klavesy[pygame.K_UP]:
            self.rychlost[0] -= 0.2 * math.sin(math.radians(self.uhel))
            self.rychlost[1] -= 0.2 * math.cos(math.radians(self.uhel))
        if klavesy[pygame.K_SPACE] and self.nabito:
            objekty.add(Strela(
                 self.rect.center[0], # x
                self.rect.center[1], # y
                self.rychlost[0] - 5 * math.sin(math.radians(self.uhel)), # vx
                self.rychlost[1] - 5 * math.cos(math.radians(self.uhel)) # vy
                ))
            self.nabito = False
        if not klavesy[pygame.K_SPACE]:
            self.nabito = True

        # Pohyb
        self.rect_original.move_ip(self.rychlost)
        if self.rect_original.center[0] > WIDTH:
            self.rect_original.move_ip(-1*WIDTH, 0)
        if self.rect_original.center[0] < 0:
            self.rect_original.move_ip(WIDTH, 0)
        if self.rect_original.center[1] > HEIGHT:
            self.rect_original.move_ip(0, -1*HEIGHT)
        if self.rect_original.center[1] < 0:
            self.rect_original.move_ip(0, HEIGHT)
        
        # Otáčení
        self.surf = pygame.transform.rotate(self.surf_original, self.uhel)
        self.rect = self.surf.get_rect(center = self.rect_original.center)

class Asteroid(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.surf_original = pygame.image.load("asteroid.png").convert()
        self.surf_original.set_colorkey((255,255,255))
        self.rect_original = self.surf_original.get_rect()
        self.rect_original.move_ip(randint(0, WIDTH),randint(0, WIDTH)) # Náhodnou pozici
        self.uhel = 90.0
        self.rychlost_rotace = randint(-5, 5) # Nahodna rotace
        self.rychlost = [random()*6-3, random()*6-3] # Nahodna rychlost
        
    
    def update(self):
        self.uhel += self.rychlost_rotace
        
        # Pohyb
        self.rect_original.move_ip(self.rychlost)
        if self.rect_original.center[0] > WIDTH:
            self.rect_original.move_ip(-1*WIDTH, 0)
        if self.rect_original.center[0] < 0:
            self.rect_original.move_ip(WIDTH, 0)
        if self.rect_original.center[1] > HEIGHT:
            self.rect_original.move_ip(0, -1*HEIGHT)
        if self.rect_original.center[1] < 0:
            self.rect_original.move_ip(0, HEIGHT)
        
        # Otáčení
        self.surf = pygame.transform.rotate(self.surf_original, self.uhel)
        self.rect = self.surf.get_rect(center = self.rect_original.center)


class Strela(pygame.sprite.Sprite):
    def __init__(self, x, y, vx, vy):
        super().__init__()
        self.surf = pygame.Surface((11, 11))
        self.rect = self.surf.get_rect()
        self.rect.move_ip(x, y)
        pygame.draw.circle(self.surf, (255, 255, 0), (5, 5), 5)
        self.surf.set_colorkey((0,0,0))
        
        self.rychlost = [vx, vy]
        self.zivotnost = 40 * 1
        
    
    def update(self):
        self.rect.move_ip(self.rychlost)
        self.zivotnost -= 1
        if self.zivotnost < 1:
            self.kill()
        

raketa = Raketa()
asteroidy = pygame.sprite.Group()
strely = pygame.sprite.Group()
for i in range(10):
    asteroid = Asteroid()
    asteroidy.add(asteroid)

def kresli_score(screen, score):
    font = pygame.font.Font(None, 50)
    text = font.render("Score %d" % score, 1, (150, 150, 150))
    screen.blit(text, (20, 20))

konec = False
score = 0

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    klavesy = pygame.key.get_pressed()
    if konec:
        continue
    klavesy = pygame.key.get_pressed()
    raketa.update(klavesy, strely)
    asteroidy.update()
    strely.update()
     
    screen.fill(BACKGROUND)

    kresli_score(screen, score)
    
    for asteroid in asteroidy:
        screen.blit(asteroid.surf, asteroid.rect)
    
    for strela in strely:
        screen.blit(strela.surf, strela.rect)
    if len(asteroidy) < 10:
        asteroid = Asteroid()
        asteroidy.add(asteroid)
        
    pygame.sprite.groupcollide(asteroidy, strely, True, True)
    
    if pygame.sprite.spritecollideany(raketa, asteroidy):
        raketa.rychlost = [0, 0]

    screen.blit(raketa.surf, raketa.rect)
    
    pygame.display.flip()
    
    if len(asteroidy) < 10:
        asteroid = Asteroid()
        asteroidy.add(asteroid)
    
    clock.tick(40)